/*
 * CSc103 Project 3: unix utilities
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours:
 */

#include <string>
using std::string;
#include <set>
using std::set;
#include <getopt.h> // to parse long arguments.
#include <cstdio> // printf

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of wc.  Supported options:\n\n"
"   -c,--bytes            print byte count.\n"
"   -l,--lines            print line count.\n"
"   -w,--words            print word count.\n"
"   -L,--max-line-length  print length of longest line.\n"
"   -u,--uwords           print unique word count.\n"
"   --help          show this message and exit.\n";

int main(int argc, char *argv[])
{
	// define long options
	static int charonly=0, linesonly=0, wordsonly=0, uwordsonly=0, longonly=0;
	static struct option long_opts[] = {
		{"bytes",           no_argument,   0, 'c'},
		{"lines",           no_argument,   0, 'l'},
		{"words",           no_argument,   0, 'w'},
		{"uwords",          no_argument,   0, 'u'},
		{"max-line-length", no_argument,   0, 'L'},
		{"help",            no_argument,   0, 'h'},
		{0,0,0,0}
	};
	// process options:
int lineCount = 1;
	int wordCount = 1;
		int byteCount = 1;
			int longCount = 1;
				int uwordCount = 1;

	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "clwuLh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'c':
				charonly = 1;
				byteCount();
				break;
			case 'l':
				linesonly = 1;
				lineCount();
				break;
			case 'w':
				wordsonly = 1;
				wordCount();
				break;
			case 'u':
				uwordsonly = 1;
				uniqCount();
				break;
			case 'L':
				longonly = 1;
				maxLCount();
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
			default:
				cout << lineCount << " " << wordCount << " " << byteCount << " " << longCount << " " << uwordCount << endl;
				return 1;
		}
	}

	/* TODO: write me... */



readChar()
	{
		char c;
		while (fread(&c,1,1,stdin))
		{
    // process c

		}
	//count the characters, ws, new line, tab  !!!
	countChar();
	//functions
	byteCount();
	lineCount();
	wordCount();
	uniqCount();
	maxLCount();





	return 0;
}
